package rs.tfzr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.tfzr.model.Rola;

public interface RolaRepository extends JpaRepository<Rola, Long> {
}
