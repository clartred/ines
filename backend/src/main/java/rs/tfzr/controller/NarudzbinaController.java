package rs.tfzr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.model.Narudzbina;
import rs.tfzr.service.NarudzbinaService;

@RestController
@RequestMapping("/narudzbina")
@CrossOrigin
public class NarudzbinaController {

    private NarudzbinaService narudzbinaService;

    @Autowired
    public NarudzbinaController(NarudzbinaService narudzbinaService) {
        this.narudzbinaService = narudzbinaService;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getOne(@PathVariable("id") Long id) {
        return new ResponseEntity(this.narudzbinaService.getOne(id), HttpStatus.OK);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getAll() {
        return new ResponseEntity(this.narudzbinaService.getAll(), HttpStatus.OK);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity edit(@RequestBody Narudzbina narudzbina) {
        return new ResponseEntity(narudzbinaService.edit(narudzbina), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity save(@RequestBody Narudzbina narudzbina) {
        return new ResponseEntity(this.narudzbinaService.insert(narudzbina), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        this.narudzbinaService.delete(id);
        return new ResponseEntity(this.narudzbinaService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/daj-narudzbine/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity dajNarudzbinePoIdKorisnika(@PathVariable("id") Long id) {
        return new ResponseEntity(this.narudzbinaService.dajNarudzbinePoIdKorisnika(id), HttpStatus.OK);
    }
}
