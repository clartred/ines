package rs.tfzr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.model.Korisnik;
import rs.tfzr.service.KorisnikService;

import java.util.List;

@RestController
@RequestMapping("/korisnik")
@CrossOrigin
public class KorisnikController {

    private KorisnikService korisnikService;

    @Autowired
    public KorisnikController(KorisnikService korisnikService) {
        this.korisnikService = korisnikService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping
    public List<Korisnik> findAll() {
        return korisnikService.findAll();
    }

    @PostMapping("/registruj-se")
    public ResponseEntity registrujSe(@RequestBody Korisnik korisnik) {
        System.out.println("korisnik: " + korisnik.getIme() + " " + korisnik.getKorisnickoIme() + " " + korisnik.getPrezime() + " " + korisnik.getSifra());
        return new ResponseEntity(korisnikService.registrujSe(korisnik), HttpStatus.OK);
    }

}
