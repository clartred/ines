package rs.tfzr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "narudzbina")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Narudzbina {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private Date datumDostave;

    @Column(nullable = false)
    private String nazivRestorana;

    @Column(nullable = false)
    private String nazivHrane;

    @Column(nullable = false)
    private String komentar;

    @Column(nullable = false)
    private Double iznosZaNaplatu;

    @Column(nullable = false)
    private Long idNarucioca;

    @Column(nullable = false)
    private Boolean moguceIsporuciti;

    public Narudzbina() {
    }

    public Narudzbina(Long id, Date datumDostave, String nazivRestorana, String nazivHrane, String komentar, Double iznosZaNaplatu, Long idNarucioca, Boolean moguceIsporuciti) {
        this.id = id;
        this.datumDostave = datumDostave;
        this.nazivRestorana = nazivRestorana;
        this.nazivHrane = nazivHrane;
        this.komentar = komentar;
        this.iznosZaNaplatu = iznosZaNaplatu;
        this.idNarucioca = idNarucioca;
        this.moguceIsporuciti = moguceIsporuciti;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatumDostave() {
        return datumDostave;
    }

    public void setDatumDostave(Date datumDostave) {
        this.datumDostave = datumDostave;
    }

    public String getNazivRestorana() {
        return nazivRestorana;
    }

    public void setNazivRestorana(String nazivRestorana) {
        this.nazivRestorana = nazivRestorana;
    }

    public String getNazivHrane() {
        return nazivHrane;
    }

    public void setNazivHrane(String nazivHrane) {
        this.nazivHrane = nazivHrane;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public Double getIznosZaNaplatu() {
        return iznosZaNaplatu;
    }

    public void setIznosZaNaplatu(Double iznosZaNaplatu) {
        this.iznosZaNaplatu = iznosZaNaplatu;
    }

    public Long getIdNarucioca() {
        return idNarucioca;
    }

    public void setIdNarucioca(Long idNarucioca) {
        this.idNarucioca = idNarucioca;
    }

    public Boolean getMoguceIsporuciti() {
        return moguceIsporuciti;
    }

    public void setMoguceIsporuciti(Boolean moguceIsporuciti) {
        this.moguceIsporuciti = moguceIsporuciti;
    }
}
