package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Korisnik;
import rs.tfzr.model.Rola;
import rs.tfzr.repository.KorisnikRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Transactional
@Service
public class KorisnikSecurityService implements UserDetailsService {

    private KorisnikRepository korisnikRepository;

    @Autowired
    public KorisnikSecurityService(KorisnikRepository korisnikRepository) {
        this.korisnikRepository = korisnikRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Korisnik korisnik = korisnikRepository.findByKorisnickoIme(username);
            if (korisnik == null) {
                return null;
            }
            return new User(korisnik.getKorisnickoIme(), passwordEncoder().encode(korisnik.getSifra()), getAuthorities(korisnik));
        } catch (Exception e) {
            throw new UsernameNotFoundException("Korisnik nije nadjen.");
        }
    }

    private Set<GrantedAuthority> getAuthorities(Korisnik korisnik) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (Rola rola : korisnik.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(rola.getType().toString());
            authorities.add(grantedAuthority);
        }
        return authorities;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
