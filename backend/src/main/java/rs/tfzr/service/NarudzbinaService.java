package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Narudzbina;
import rs.tfzr.repository.NarudzbinaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class NarudzbinaService {

    private NarudzbinaRepository narudzbinaRepository;

    @Autowired
    public NarudzbinaService(NarudzbinaRepository narudzbinaRepository) {
        this.narudzbinaRepository = narudzbinaRepository;
    }

    public Narudzbina getOne(Long id) {
        return narudzbinaRepository.getOne(id);
    }

    public List<Narudzbina> getAll() {
        return narudzbinaRepository.findAll();
    }

    public void delete(Long id) {
        narudzbinaRepository.deleteById(id);
    }

    public Narudzbina edit(Narudzbina narudzbina) {
        if (narudzbina.getIznosZaNaplatu() > 999) {
            narudzbina.setMoguceIsporuciti(true);
        } else {
            narudzbina.setMoguceIsporuciti(false);
        }
        return narudzbinaRepository.save(narudzbina);
    }

    public Narudzbina insert(Narudzbina narudzbina) {
        if (narudzbina.getIznosZaNaplatu() > 999) {
            narudzbina.setMoguceIsporuciti(true);
        } else {
            narudzbina.setMoguceIsporuciti(false);
        }
        return narudzbinaRepository.save(narudzbina);
    }

    public List<Narudzbina> dajNarudzbinePoIdKorisnika(Long id) {
        return this.narudzbinaRepository.findNarudzbinaByIdNarucioca(id);
    }

}
