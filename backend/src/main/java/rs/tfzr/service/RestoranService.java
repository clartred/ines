package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Restoran;
import rs.tfzr.repository.RestoranRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class RestoranService {

    private RestoranRepository restoranRepository ;

    @Autowired
    public RestoranService(RestoranRepository restoranRepository) {
        this.restoranRepository = restoranRepository;
    }

    public Restoran dajJedan(Long id){
        return restoranRepository.getOne(id);
    }

    public List<Restoran> dajSve(){
        return restoranRepository.findAll();
    }

    public Restoran dodajRestoran(Restoran restoran){
        return restoranRepository.save(restoran);
    }

    public void delete(Long id){
        restoranRepository.deleteById(id);
    }


}
