
INSERT INTO `rola`(`id`, `type`) VALUES (1, 'ROLE_ADMIN');
INSERT INTO `rola`(`id`,`type`) VALUES (2, 'ROLE_USER');

INSERT INTO `korisnik`(`id`, `sifra`,`korisnicko_ime`, `ime`, `prezime`) VALUES (3, 'admin','admin', 'admin','admin');

INSERT INTO `korisnik`(`id`, `sifra`,`korisnicko_ime`, `ime`, `prezime`) VALUES (4, 'user','user', 'user','user');

INSERT INTO `korisnik_roles`(`user_id`,`role_id`)VALUES(3, 1);

INSERT INTO `korisnik_roles`(`user_id`,`role_id`)VALUES(4, 2);

INSERT INTO `restoran`(`id`,`naziv`)VALUES(77, 'Camel');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(78, 'Domacin');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(79, 'Arena ZR');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(80, 'Kafemat');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(81, 'Lion Pub');
INSERT INTO `restoran`(`id`,`naziv`)VALUES(82, 'Zanatlija');
