import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../shared/dialog/dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  korisnickoIme: string;
  sifra: string;

  constructor(
    private loginService: LoginService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if(this.loginService.daLiJeKorisnikUlogovan()){
      this.router.navigate(['home']);
    }
  }

  login() {

    if (!this.korisnickoIme || this.korisnickoIme === '') {
      this.openDialog('Morate uneti korisnicko ime', '350px', '300px', false);
    } else if (!this.sifra || this.sifra === '') {
      this.openDialog('Morate uneti sifru', '350px', '300px', false);
    } else {
      this.ulogujSe();
    }
  }

  ulogujSe() {
    this.loginService.login(this.korisnickoIme, this.sifra);
  }

  registrujem() {
    this.router.navigate(['register']);
  }

  openDialog(text: string, height: string, width: string, action: boolean) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height,
      data: text
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  redirectToRegister(){
    this.router.navigate(['register']);
  }

}
