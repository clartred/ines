import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public korisnikJeUlogovan = false;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.loginService.daLiJeKorisnikUlogovan()) {
      this.korisnikJeUlogovan = true;
      this.router.navigate(['home']);
    } else {
      this.korisnikJeUlogovan = false;
    }
    this.gledajPromeneNaRuti();
  }

  gledajPromeneNaRuti() {
    setInterval(() => {
      this.router.events.subscribe(() => {
        this.korisnikJeUlogovan = this.loginService.daLiJeKorisnikUlogovan();
      });
    }, 2500)

  }

  ulogujSe() {
    this.router.navigate(['login']);
  }

  izlogujSe() {
    this.loginService.logout();
  }

  redirectToHome(){
    this.router.navigate['home'];
  }

}
