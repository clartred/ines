export class Korisnik {

    id: string;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    sifra: string;
}
